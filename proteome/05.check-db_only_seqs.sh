#!/bin/bash
blat -prot XENTR_xenTro10.XB2023_04.raw.prot_combined.fa XENTR_xenTro10.XB2023_04.raw.prot_UniProt_only.fa XENTR_xenTro10.XB2023_04.raw.prot_UniProt_only.2prot_combined.pblat_psl
blat -prot XENTR_xenTro10.XB2023_04.raw.prot_combined.fa XENTR_xenTro10.XB2023_04.raw.prot_NCBI_only.fa XENTR_xenTro10.XB2023_04.raw.prot_NCBI_only.2prot_combined.pblat_psl
./psl-to-top2.py XENTR_xenTro10.XB2023_04.raw.prot_NCBI_only.2prot_combined.pblat_psl
./psl-to-top2.py XENTR_xenTro10.XB2023_04.raw.prot_UniProt_only.2prot_combined.pblat_psl

awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' XENTR_xenTro10.XB2023_04.raw.prot_NCBI_only.2prot_combined.pblat_top2 | sort -k 6 -nr > XENTR_xenTro10.XB2023_04.raw.prot_NCBI_only.2prot_combined.pblat_best
awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' XENTR_xenTro10.XB2023_04.raw.prot_UniProt_only.2prot_combined.pblat_top2 | sort -k 6 -nr > XENTR_xenTro10.XB2023_04.raw.prot_UniProt_only.2prot_combined.pblat_best
