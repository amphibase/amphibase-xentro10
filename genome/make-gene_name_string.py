#!/usr/bin/env python3

filename_gff3 = 'XENTR_ncbi104.gene.coding.gff3'
nearby_genes = 5

gene_list = dict()
f_gff3 = open(filename_gff3, 'r')
for line in f_gff3:
    tokens = line.strip().split("\t")
    chr_id = tokens[0]
    tmp_start = int(tokens[3])
    tmp_tag = tokens[8]
    if chr_id not in gene_list:
        gene_list[chr_id] = dict()
    gene_list[chr_id][tmp_tag] = tmp_start
f_gff3.close()

print("#ChrID\tLeftGenes\tQueryGene\tRightGenes")
for tmp_chr_id in sorted(gene_list.keys()):
    sorted_gene_list = sorted(gene_list[tmp_chr_id].keys(),
                              key=gene_list[tmp_chr_id].get)

    for i in range(0, len(sorted_gene_list)):
        # ID=GeneID:116408214;Name=LOC116408214;biotype=protein_coding
        tmp_q_gene = sorted_gene_list[i].replace(';biotype=protein_coding', '')
        tmp_q_gene = tmp_q_gene.replace('ID=', '')
        tmp_left_idx = max([i-nearby_genes, 0])
        tmp_right_idx = min([len(sorted_gene_list), i+1+nearby_genes])
        tmp_left_gene_names = [tmp.split(';')[1].replace('Name=', '')
                               for tmp in sorted_gene_list[tmp_left_idx:i]]
        tmp_right_gene_names = [tmp.split(';')[1].replace('Name=', '')
                                for tmp in sorted_gene_list[i+1:tmp_right_idx]]
        print("%s\t%s\t%s\t%s" % (tmp_chr_id, ','.join(tmp_left_gene_names),
                                  tmp_q_gene, ','.join(tmp_right_gene_names)))
