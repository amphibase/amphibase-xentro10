#!/usr/bin/env python3

filename_refseq = 'GCF_000004195.4_UCB_Xtro_10.0_protein_NR.fa'
# >NP_001001190.1
# MRLLAGAGLCLALAALALLAVALSTDHWYETDARRHRDRCRKPGGKRNDPGYMYTPGQHLPLRGEPPSSRIRSPRGGEPG
# GVRMISRAEDLGVRGLRERPTGARDLPLSRPYLATDPHCSRRFNSTVSGLWRKCHRDGFDKDTEELILKGIVERCTSVRY

filename_xb = 'XENTR_xenTro10.XB2023_03.pep_raw.fa'
# >sell|XB:XB-GENE-983824|GeneID:100498512|XBXT10g017325
# MHQKNSKICLTFVWRALKVFVTLFVFNAMLQVSPVQCWTYHYSVENMNYDQARVFCKNSYTDLVAIQNKEEIEYLNKAIP
# HNPTYYWIGIRKISGTWTWVGTNKTLTEEAQNWGKGEPNNKKNKEDCVEIYIQRQNDTGKWNDDACIKKKRALCYTASCN

filename_out_base = 'XENTR_xenTro10.XB2023_03.pep'


def read_fasta(tmp_filename):
    rv = dict()
    f = open(tmp_filename, 'r')
    for line in f:
        if line.startswith('>'):
            tmp_h = line.strip().lstrip('>')
            rv[tmp_h] = []
        else:
            rv[tmp_h].append(line.strip())
    f.close()
    return rv


refseq_list = read_fasta(filename_refseq)
xb_list = read_fasta(filename_xb)

refseq2h = dict()
for tmp_h, tmp_seq_list in refseq_list.items():
    tmp_seq = ''.join(tmp_seq_list)
    refseq2h[tmp_seq] = tmp_h

f_out = open('%s.fa' % filename_out_base, 'w')
f_no_name = open('%s_NoName.fa' % filename_out_base, 'w')
f_no_xb = open('%s_NoXB.fa' % filename_out_base, 'w')
f_no_refseq = open('%s_NoRefSeq.fa' % filename_out_base, 'w')

# bad gene names from manual curation
bad_names = ['wi2-2373i1.2', 'prss8l100494289', 'pmt100124841',
             '“or52l1l100488220', 'or52p1l100487911',
             'pinopsin100496420', 'ig100492381',
             'fucolectinloc100491211']

for tmp_h, tmp_seq_list in xb_list.items():
    tmp_seq = ''.join(tmp_seq_list)
    if tmp_seq in refseq2h:
        tmp_prot_id = refseq2h[tmp_seq]
        if tmp_h.find('XB:NA') < 0 and tmp_h.find('XB-GENEPAGE') < 0:
            tmp_gene_name = tmp_h.split('|')[0]
            if tmp_gene_name.startswith('LOC') or\
               tmp_gene_name.startswith('MGC') or\
               tmp_gene_name.startswith('xbg10') or\
               tmp_gene_name.find('provisional') >= 0 or\
               tmp_gene_name.find('-like') >= 0 or\
               tmp_gene_name in bad_names:
                f_no_name.write('>%s|%s\n%s\n' % (tmp_h, tmp_prot_id, tmp_seq))
            else:
                f_out.write('>%s|%s\n%s\n' % (tmp_h, tmp_prot_id, tmp_seq))
        else:
            f_no_xb.write('>%s|%s\n%s\n' % (tmp_h, tmp_prot_id, tmp_seq))
    else:
        f_no_refseq.write('>%s|NA\n%s\n' % (tmp_h, tmp_seq))

f_out.close()
f_no_name.close()
f_no_xb.close()
f_no_refseq.close()
