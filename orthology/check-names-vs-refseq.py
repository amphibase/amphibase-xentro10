#!/usr/bin/env python3
import sys

filename_list = sys.argv[1]

name2id = dict()
f_fa = open('../proteome/XENTR_xenTro10.XB2023_04.prot_all.fa', 'r')
#>tmem161a|NP_001001896.2|NM_001001896.2|GeneID:431674|XB-GENE-1004051
for line in f_fa:
    if line.startswith('>'):
        tokens = line.strip().split()[0].lstrip('>').split('|')
        gene_name = tokens[0]
        gene_id = tokens[3]
        xb_gene_id = tokens[4]
        name2id[gene_name] = {'gene_id': gene_id, 'xb_gene_id': xb_gene_id}
f_fa.close()

diopt_gene_list = []

#f_list = open('Nivitha_DIOPT.XT-HS.2023_03_29.all.txt', 'r')
f_list = open(filename_list, 'r')
for line in f_list:
    if line.startswith('#'):
        continue
    tmp_gene_name = line.strip().split()[0]
    diopt_gene_list.append(tmp_gene_name)
    if tmp_gene_name not in name2id:
        print("%s\t%s\t%s" % (line.strip(), "NivithDIOPT_only", "N/A"))
    else:
        print("%s\t%s\t%s" % (line.strip(), name2id[tmp_gene_name]['gene_id'],name2id[tmp_gene_name]['xb_gene_id']))
    #    print("Both\t%s" % tmp_gene_name)
f_list.close()

#for tmp_name in sorted(name2id.keys()):
#    if tmp_name not in diopt_gene_list:
#        print("RefSeq_only\t%s" % tmp_name)
