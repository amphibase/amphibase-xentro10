# Release 2020_04

Gene name assignment for X. tropicalis proteome. 

## Procedures.

1. Get Human vs X. tropicalis orthology information 
(produced by Nivitha Sundararaj, XenBase)
 - Mod_parsed_XT_HS_All_Diopt_Tool_Data.txt.gz
 - Needs to clarify the source of the analysis (protein sequences).

2. Summarize the information by counting the number of supports 
(differet orthology databases) and comparing the best human
ortholog candidates to the current X. tropicalis gene name. 
If the names are not matched, report it in the "for_review" file.
If the names are matched (or partially matched, such as "abo.1" and "ABO"),
if the best human ortholog is supported less than two databases, 
compared to the second best human ortholog, 
report it also in the "for_review" file. 
Otherwise, report it in the "good" file. 

 - A script for analsis: evaluate-Nivitha-DIOPT.py
 - Output1: Nivitha_DIOPT.XT-HS.2023_03_29.all.txt
 - Output2: Nivitha_DIOPT.XT-HS.2023_03_29.for_review.txt
 - Output3: Nivitha_DIOPT.XT-HS.2023_03_29.good.txt

3. Manually review "for_review" and "good" file, and make the list of genes
for changing their gene names or for discarding from the reference release.
  - If multiple genes are mapped to a single human gene, mark them all as "DISCARD".
  - Output1 (to change gene name): CHANGE.2023_03.txt (raw data) CHANGE.na:mes.2023_03.txt (name only)
  - Output2 (retain it as is; need to be reviewed for the next release): REVIEW.2023_03.txt
  - Output3 (to discard from the reference): DISCARD.2023_03.txt 

