#!/usr/bin/env python3

f_gff = open('../genome/XENTR.ncbi104.gene.gff3.converted.coding', 'r')
for line in f_gff:
    if line.startswith('#'):
        continue
    tokens = line.strip().split("\t")
    tmp_chr = tokens[0]
    tmp_old_name = tokens[8].split(';')[1].replace('Name=', '')
    if tmp_old_name.startswith('c') and tmp_old_name.find('orf') >= 0:
        if tmp_old_name.find('h') >= 0:
            continue
        tmp_new_name = "%s%s" % (tmp_chr.replace('chr', 'c'), tmp_old_name.replace("c", "h"))
        print("%s\t%s" % (tmp_old_name, tmp_new_name))
f_gff.close()
